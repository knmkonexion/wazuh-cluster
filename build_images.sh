#!/bin/bash

REPO=wazuh
VERSION=3.12.0_7.6.1

echo 'Building images locally'

# kibana
echo 'building kibana'
docker build -t "${REPO}/${REPO}-kibana:${VERSION}" kibana/

# GENERATED OK
# wazuh
# echo 'building wazuh'
# docker build -t "${REPO}/${REPO}:${VERSION}" wazuh/

# nginx
# echo 'building nginx'
# docker build -t "${REPO}/${REPO}-nginx:${VERSION}" nginx/

# elasticsearch
# echo 'building elasticsearch'
# docker build -t "${REPO}/${REPO}-elasticsearch:${VERSION}" elasticsearch/
